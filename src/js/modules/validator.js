//
'use strict'
module.exports = Validator;

function Validator(value = '', rules = {}) {

	this.value = value;
	this.rules = rules;
	this.failRules = [];

}

// update value to validate
Validator.prototype.update = function (val = '') {
	this.value = val;
}

//update validation rule set
Validator.prototype.setRules = function (rules = {}) {
	this.rules = rules;
}

//perform validation
Validator.prototype.check = function () {
	this.failRules = [];
	var rules = Object.keys(this.rules);

	//try to find and execute validators for validation...
	rules.forEach(
		(rule) => {
			if (typeof this[rule] === 'function') {
				//call validator with params from rule
				if (!this[rule](this.rules[rule])) {
					this.failRules.push(rule);
				}

			} else {
				console.warn('can\'t find validator:', rule)
			}
		}
	)
	return this.failRules.length == 0 ? true : false;
}


//Validators functions
Validator.prototype.len = function (param) {
	console.debug('checking len with', param);
	if (typeof this.value !== 'string'
		||
		this.value.length !== param
	) {
		return false;
	}
	return true;
}

Validator.prototype.maxlen = function (param) {
	console.debug('checking maxlen with', param);

	if (typeof this.value !== 'string'
		||
		this.value.length >= param
	) {
		return false;
	}
	return true;
}

Validator.prototype.type = function (param) {
	console.debug('checking type with', param);

	if (typeof this.value !== param
	) {
		return false;
	}
	return true;
}

Validator.prototype.isString = function (_param) {
	console.debug('checking for isString', _param);

	if (typeof this.value !== 'string') {
		return false;
	}
	return true;
}

Validator.prototype.regEx = function (param) {
	console.debug('checking regEx rule with', param);

	if (typeof this.value == 'string') {
		if (this.value.search(param) == -1)
			return false;
	}

	return true;
}

