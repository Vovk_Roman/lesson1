'use strict'

const Emitter = require('component-emitter');

module.exports = function (wrapper, options) {

	return new Slider(wrapper, options);
}

function Slider(wrapper = null, options = {}) {

	const that = this;

	//model with default params
	this.model = {
		wrapper: '',
		height: 200,
		width: 300,
		slideCount: 4,
		autoplay: true,
		mixInOut: true,
		slides: [
			{
				src: 'http://lorempixel.com/300/200/nature/1/',
				showTime: 7000,
			},
			{
				src: 'http://lorempixel.com/300/200/nature/2/',
				showTime: 3000,
			},
			{
				src: 'http://lorempixel.com/300/200/nature/3/',
				showTime: 3000,
			},
			{
				src: 'http://lorempixel.com/300/200/nature/4/',
				showTime: 5000,
			}
		],
		//state
		currentSlide: 0,
		inAnimation: false
	}

	//setup wrapper
	if (typeof wrapper == 'string') {
		this.model.wrapper = document.querySelector(wrapper)
	} else {
		this.model.wrapper = wrapper;
	}
	//console.log(wrapper,this.model.wrapper);

	//setup options
	this.options = {};
	this.parseOptions(options);


	this.cleanup();
	this.show();

	// autoplayer
	if (this.model.autoplay) {
		//setup handler
		this.on('newSlide',
			() => {
				setTimeout(() => { this.nextSlide() }, this.model.slides[this.model.currentSlide].showTime)
			})
		//first timeout should be executed manually
		setTimeout(() => { this.nextSlide() }, this.model.slides[this.model.currentSlide].showTime);
	}

	// protect our scroller
	let observer = new MutationObserver(
		(mutations) => {
			mutations.forEach((mutation) => {
				if (!mutation.addedNodes.length && mutation.removedNodes.length) {
					if (!this.model.inAnimation) {
						//redraw if poshsible
						this.cleanup();
						this.show();
					}
				}
			})
		});
	observer.observe(this.model.wrapper, { subtree: true, attributes: false, childList: true, characterData: false })
}

//clear wrapper content
Slider.prototype.cleanup = function () {
	this.model.wrapper.innerHTML = '';
}

//fill wrapper with content
Slider.prototype.show = function () {


	var sliderEl = document.createElement('div');
	sliderEl.dataset.role = 'slider-content';

	//setup basic layout	
	sliderEl.style.height = this.model.height;
	sliderEl.style.width = this.model.width;

	// create slides
	this.model.slides.forEach((v, i, a) => {
		//create
		let content = '<div></div>';
		sliderEl.insertAdjacentHTML('beforeend', content);

		//setup
		let currentDiv = sliderEl.querySelector(`div:nth-of-type(${i + 1})`);
		currentDiv.style.background = (`url(${v.src}) top center /cover no-repeat`);
		currentDiv.style.height = this.model.height;
		currentDiv.style.width = this.model.width;
		//		currentDiv.style.opacity = 0;
		currentDiv.innerHTML = `div ${i}`;

		//setup initial classes
		if (i == 0) {
			currentDiv.classList.toggle('anim', true);
			currentDiv.classList.toggle('right', false);
		}
		else { currentDiv.classList.toggle('right', true); }
		//setup data-*
		currentDiv.dataset.role = 'slide';

	})

	//create controls
	sliderEl.insertAdjacentHTML('beforeend', '<div class="controls" data-role=controls> </div>');

	let controlsEl = sliderEl.querySelector('div.controls')
	this.model.slides.forEach((v, i, a) => {
		i != this.model.currentSlide ?
			controlsEl.insertAdjacentHTML('beforeend', '<div class="slide"></div>')
			:
			controlsEl.insertAdjacentHTML('beforeend', '<div class="current"></div>');
	})

	this.model.wrapper.insertAdjacentElement('beforeend', sliderEl);
}

Slider.prototype.parseOptions = function (options) {
	
	const params =
		//use lower case please!
		[
			'autoplay',
			'slidedelay'
		]

	for (let option in options) {
		console.debug(`parsing option: ${option}=${options[option]}`)
		if (params.indexOf(option.toLowerCase()) >= 0) {
			this.options[option] = options[option];
		}
		else {
			
			console.warn(`Unknown option: ${option}=${options[option]}`);
		}
	}
}

Slider.prototype.prevSlide = function () {
	if (!this.model.inAnimation) {
		let currentSlide = this.model.currentSlide;
		let nextSlide = this.model.currentSlide - 1;
		if (nextSlide < 0) { nextSlide = this.model.slideCount - 1; }
		this.animate(currentSlide, nextSlide, 'ltr');
		this.model.currentSlide = nextSlide;
		this.emit('prevSlide');
	}
}

Slider.prototype.nextSlide = function () {
	if (!this.model.inAnimation) {
		let currentSlide = this.model.currentSlide;
		let nextSlide = this.model.currentSlide + 1;
		if (nextSlide == this.model.slideCount) { nextSlide = 0; }
		this.animate(currentSlide, nextSlide, 'rtl');
		this.model.currentSlide = nextSlide;
		this.emit('nextSlide');
	}
}

Slider.prototype.animate = function (currentSlide, nextSlide, dir = 'rtl') {
	
	this.emit('animationStart',`animating from ${currentSlide} to ${nextSlide}`);
	//set animation flag
	this.model.inAnimation = true;

	var sliderElements = this.model.wrapper.querySelectorAll(`div[data-role="slider-content"]>div[data-role="slide"]`);
	
	switch (dir) {
		case 'rtl':
			//move slides pack to right
			sliderElements.forEach((_v, i) => {
				if (i != currentSlide) {
					sliderElements[i].classList.toggle('anim', false);
					sliderElements[i].classList.toggle('left', false);
					sliderElements[i].classList.toggle('right', true);
				}
			});
			//recalculate NextSlide DOM
			sliderElements[nextSlide].offsetTop;
			//toggle classes for transition
			sliderElements[currentSlide].classList.toggle('anim', true);
			sliderElements[currentSlide].classList.toggle('left', true);
			sliderElements[nextSlide].classList.toggle('anim', true);
			sliderElements[nextSlide].classList.toggle('right', false);
			break;

		case 'ltr':
			//move slides stack to left
			sliderElements.forEach((_v, i) => {
				if (i != currentSlide) {
					sliderElements[i].classList.toggle('anim', false);
					sliderElements[i].classList.toggle('left', true);
					sliderElements[i].classList.toggle('right', false);
				}

			});
			//recalculate NextSlide DOM
			sliderElements[nextSlide].offsetTop;
			//toggle classes for transition
			sliderElements[currentSlide].classList.toggle('anim', true);
			sliderElements[currentSlide].classList.toggle('right', true);
			sliderElements[nextSlide].classList.toggle('anim', true);
			sliderElements[nextSlide].classList.toggle('left', false);
			break;
	}

	//update controls
	let controlElements = this.model.wrapper.querySelectorAll('div[data-role="controls"]>div');

	controlElements[currentSlide].classList.toggle('current', false);
	controlElements[currentSlide].classList.toggle('slide', true);

	controlElements[nextSlide].classList.toggle('current', true);
	controlElements[nextSlide].classList.toggle('slide', false);

	// we need named function to remove handler by name
	// and closure to access "this.xxx" inside event handler
	sliderElements[nextSlide].addEventListener('transitionend',
		((that) => {
			return function evh(e) {
				sliderElements[nextSlide].removeEventListener('transitionend', evh)
				that.model.inAnimation = false;
				that.emit('animationEnd');
				that.emit('newSlide');
			}
		})(this));
}

Slider.prototype.dumpConfig = function () {
	console.log(`options: ${this.options}`);

}

Slider.prototype.contentTemplate = '<div class="" data-role="slider-content"></div>';

// add  emitter
Emitter(Slider.prototype);