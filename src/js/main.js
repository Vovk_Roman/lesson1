//FrontEnd Scripts
'use strict'

require("babel-polyfill");
var match = require("matches-selector");
var Validator = require("./modules/validator");
require("element-closest");

const sliderFactory = require('./modules/slider');

const slider = sliderFactory('[data-role="slider-wrapper"]',{autoplay:true,slideDelay:3,blbla:'haha'});

slider.on('nextSlide', (e)=>{console.log('nextSlide event!:',e)} )
slider.on('prevSlide', (e)=>{console.log('prevSlide event!:',e)} )
slider.on('animationStart', (e)=>{console.log('animationStart event!:',e)} )
slider.on('animationEnd', (e)=>{console.log('animationEnd event!:',e)} )



slider.dumpConfig(); 


document.getElementById('btnnext').addEventListener('click', ()=>{slider.nextSlide()});
document.getElementById('btnplay').addEventListener('click', ()=>{
	slider.model.autoplay=!slider.model.autoplay;
	if (slider.model.autoplay) {slider.nextSlide()}
});

document.getElementById('btnprev').addEventListener('click', ()=>{
 {slider.prevSlide()}
});


myApp(document.querySelector(
	'[data-role="cards-wrapper"]'),
	document.getElementById('getusersbtn')
);

function myApp(crdWrapper,crdBtn,sliderWrapper,sliderBtn) {
	
	const cardsWrapper = crdWrapper;
	const getMoreButton = crdBtn;
	
	//sliderBtn
	
	// {element_to_animate(data-role_ID), animation_to_apply}
	//all animations from animate.css
	const animations = [
		{
			element: 'usercard',
			animation: 'rubberBand'
		},
		{
			element: 'usercard',
			animation: 'flash'
		},
		{
			element: 'usercard',
			animation: 'jello'
		},
		{
			element: 'firstName',
			animation: 'flash'
		},
		{
			element: 'secondName',
			animation: 'flash'
		}


	]
	
	getMoreButton.addEventListener('click', getSomeCards);

	cardsWrapper.addEventListener('click',
		function (e) {
			//respnd only on button clicks!
			if (match(e.target, 'button')) {
				switch (e.target.dataset.role) {
					case 'btnclose': {
						var userCard = e.target.closest('[data-role="usercard"]');
						userCard.closest('[data-role="cards-wrapper"]').removeChild(userCard)
						return;
					}
					case 'btnanimate': {
						var userCard = e.target.closest('[data-role="usercard"]');
						animateCard(userCard);
						return;
					}
					case 'btnedit': {
						var userCard = e.target.closest('[data-role="usercard"]');
						editCard(userCard);
						return;
					}
					case 'btnsave': {
						var userCard = e.target.closest('[data-role="usercard"]');
						saveCard(userCard);
						return;
					
					}
				}
			}
		}
	);

	

	async function animateCard(card) {

		for (let step in animations) {
			await animStep(step);
		}
		//that's all! Cool!
	
		//let's start with resolved Promise.
		// var animPromise = Promise.resolve(0);

		// for (var anim in animations) {
		// 	animPromise = animPromise.then(animStep);
		// 	console.debug(`adding animstep: ${animations[anim].animation}`);
		// }

		// animPromise = animPromise.catch(console.error);
		
		function animStep(step) {

			return new Promise((resolve, reject) => {

				//find element to animate
				var el = null;
				card.dataset.role == animations[step].element ?
					el = card
					:
					el = card.querySelector(`[data-role="${animations[step].element}"]`)
				
				if (!el) {reject(`can't animate "${animations[step].element}" element. Element not found`);}
				
				
				//addEventListener
				el.addEventListener('animationend', evh);
				//setup animation
				el.classList.add(animations[step].animation);	
				el.classList.add('animated');
				
				

				function evh(ev) {
					//remove anim class
					ev.target.classList.remove(ev.animationName);
					console.debug(`anim ${ev.animationName} finished on ${ev.target.dataset.role}`);
					//remove EventHandler
					el.removeEventListener('animationend', evh);
					resolve(++step);
				}
			}) //end promise 
		}
	}

	function editCard(card) {
		var fieldSet = card.querySelectorAll('span[data-role]');
		//cache request for future use
		getURL('/fieldinput.html', true)
			.then((result) => {
				Array.from(fieldSet).forEach(
					(field) => {
						var fieldText = field.innerHTML;
						field.innerHTML = result;
						field.querySelector('input').value = fieldText;
						
					})
			})
			.then(()=>{
				card.querySelector('button[data-role="btnedit"]').classList.toggle('hide');
				card.querySelector('button[data-role="btnsave"]').classList.toggle('hide');
			})
			.catch(console.error);
	}

	function saveCard(card){
		var validator = new Validator();
		var fieldSet=card.querySelectorAll('span[data-role]');
		var fieldsValid=true;
		
		Array.from(fieldSet).forEach(
			(field)=>{
					var fieldText=field.querySelector('input').value
					
					// prepare for validation...
					switch(field.dataset.role) {
						case 'firstName' : {
							
							// nobreak, the same as for secondName;
						}
						case 'secondName' : {
							validator.setRules({maxlen:20,regEx:/^[A-Z][a-z]+$/});
							break;
						}
						case 'phone' : { 
							validator.setRules({maxlen:20,regEx:/^\d{7,12}$/});
							break;
						}
						case 'email' : { //Честно нагуглено. :-) http://emailregex.com/
							validator.setRules({maxlen:100,regEx:/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/});
							break;
						}
						default: { validator.setRules({}); }
					}
					
					validator.update(fieldText);
					
					//let's check...
					fieldsValid = fieldsValid && validator.check();
					
					// mark-unmark invalidates fields.
					if (validator.check()) 
					{field.classList.add('has-success')
					 field.classList.remove('has-error')
						}
					else
					{ field.classList.add('has-error')
						field.classList.remove('has-success')
			}

				});
		//all ok - switch to card mode.		
		if (fieldsValid) {			
		var fieldSet=card.querySelectorAll('span[data-role]');
		Array.from(fieldSet).forEach(
			(field)=>{
					var fieldText=field.querySelector('input').value
					field.innerHTML=fieldText;
					});
		card.querySelector('button[data-role="btnedit"]').classList.toggle('hide');
		card.querySelector('button[data-role="btnsave"]').classList.toggle('hide');
	}
	}


	async function getSomeCards() {
		try {
			let userDB = await	getURL(`http://${window.location.host}/users.json`,false); //don't cache JSON
			let cards = await	getCards(userDB);
			showCards(cards);
		} catch(e) {
			console.error(e);
		} 
	}

	//stores request cache in getURL.cache object
	function getURL(url, useCache) {

		if (!getURL.cache) getURL.cache = {};


		if (useCache) {
			if (getURL.cache[url]) {
				return Promise.resolve(getURL.cache[url]);
			}
		}



			return new Promise(function (resolve, reject) {

				var req = new XMLHttpRequest();
				req.addEventListener('error', reqOnError);
				req.addEventListener('readystatechange', reqStateChange)
				req.open('GET', url)
				req.send();

				function reqOnError() { reject(new Error('Error getting JSON')); }

				function reqStateChange() {
					if (this.readyState == 4) {
						if (this.status == 200) {
							if (useCache) {
								getURL.cache[url] = this.responseText;
							}
							resolve(this.responseText);

						}
						else
							reject(new Error(`Error getting JSON: status ${this.status}`));
					}
				}

			})

		
	}

	// вернет из Promise array  ( usersDB с usersDB.template = HTML код темплейтов)
	function getCards(usersJSON) {

		var cardPromises = [];

		var usersDB = JSON.parse(usersJSON);

		usersDB.forEach((user) => {
			cardPromises.push(getURL(user.template, true)
				.then(function appendTemplate(template) {
					user.tepmlateHTML = template;
					return Promise.resolve(user);
				}))
		})
		return Promise.all(cardPromises);
	}

	function showCards(users) {

		users.forEach((user) => {
		
			cardsWrapper.insertAdjacentHTML('beforeend',user.tepmlateHTML); //innerHTML += user.tepmlateHTML;

			var emptyCard = cardsWrapper.querySelector('[data-role="emptycard"]');

			//fill all fields (if possible)
			Array.from(emptyCard.querySelectorAll('span[data-role]'))
				.forEach(el => {
					el.innerHTML = user[el.dataset.role] || '---unknown field---';
				});

			//insert picture (or avatar)
			var picWrapper = emptyCard.querySelector('div[data-role]');
			var img = new Image(200, 300);
			img.src = user[picWrapper.dataset.role] || `http://${window.location.host}/images/avatar.jpg`;
			img.classList.add('img-responsive');
			img.classList.add('img-rounded');
			picWrapper.appendChild(img);
			
			//mark card as processed
			emptyCard.dataset.role='usercard';
			
			// hack V2
			emptyCard.offsetTop; // to redraw DOM. maybe offsetLeft, etc..
			emptyCard.classList.add('show1');

			//hack for fade-in
			//   (function(el) {
			//   	setTimeout( () => el.classList.add('show1'),100)})(emptyCard);
		})
	}	
}